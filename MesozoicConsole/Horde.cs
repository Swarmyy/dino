using System;
using System.Collections.Generic;

namespace MesozoicConsole
{
    public class Horde
    {
        List<Dinosaur> Dinosaurs = new List<Dinosaur>();

        public Horde()
        {

        }
        public void add(Dinosaur dino)
        {
            Dinosaurs.Add(dino);
        }
        public void remove(Dinosaur dino)
        {
            Dinosaurs.Remove(dino);
        }
        public string intro()
        {
            string text = "";
            foreach (Dinosaur dino in Dinosaurs)
            {
                text += string.Format("{0}\n",dino.sayHello());
            }
            return text;
        }
    }
}