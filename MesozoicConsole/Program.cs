﻿using System;
using System.Collections.Generic;

namespace MesozoicConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Dinosaur louis=Laboratory.createDinosaur("Louis","Stegosaure");
            Console.WriteLine(louis.sayHello());
            Dinosaur henry=Laboratory.createDinosaur("henry","diplodocus");
            Console.WriteLine(louis.sayHello());
            Console.WriteLine(henry.sayHello());
            
            /*
            List<Dinosaur> dinosaurs = new List<Dinosaur>();

            dinosaurs.Add(louis); //Append dinosaur reference to end of list
            dinosaurs.Add(nessie);

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.getName());
            }

            dinosaurs.RemoveAt(1); //Remove dinosaur at index 1

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.getName());
            }

            dinosaurs.Remove(louis);

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.getName());
            */
        }
    }
}
