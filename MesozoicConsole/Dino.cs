using System;

namespace MesozoicConsole
{
    public class Dinosaur
    {
        protected string name;
        protected static string specie;
        private int age;

        public Dinosaur(string name, int age, string specie)
        {
            this.name = name;
            Dinosaur.specie = specie;
            this.age = age;
        }
        public string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, Dinosaur.specie, this.age);
        }
        public string roar()
        {
            return string.Format("Roar");
        }
        public string getName()
        {
            return this.name;
        }
        public int getAge()
        {
            return this.age;
        }
        public string getSpecie()
        {
            return Dinosaur.specie;
        }
        public void setName(string name)
        {
            this.name = name;
        }
        public void setSpecie(string specie)
        {
            Dinosaur.specie = specie;
        }
        public void setAge(int age)
        {
            this.age = age;
        }
        public string hug(Dinosaur dinosaur)
        {
            return string.Format("Je suis {0} et je fais un calin a {1}.", this.name, dinosaur.name);
        }
    }
}