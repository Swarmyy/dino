using System;

namespace MesozoicConsole
{
    public class Laboratory{
        public static Dinosaur createDinosaur(string name,string specie){
            return new Dinosaur(name,0,specie);
        }
    }
}