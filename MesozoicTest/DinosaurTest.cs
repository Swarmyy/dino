using System;
using Xunit;
using MesozoicConsole;

namespace MesozoicTest
{
    public class UnitTest1
    {
        [Fact]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", 12, "Stegausaurus");

            Assert.Equal("Louis", louis.getName());
            Assert.Equal("Stegausaurus", louis.getSpecie());
            Assert.Equal(12, louis.getAge());
        }

        [Fact]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", 12, "Stegausaurus");
            Assert.Equal("Roar", louis.roar());
        }

        [Fact]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", 12, "Stegausaurus");
            Assert.Equal("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
        }
        [Fact]
        public void TestDinosaurHug()
        {
            Dinosaur louis = new Dinosaur("Louis", 12, "Stegausaurus");
            Dinosaur nessie = new Dinosaur("Nessie", 34, "Diplodocus");
            Assert.Equal("Je suis Louis et je fais un calin a Nessie.", louis.hug(nessie));
        }
        /*[Fact]
        public void TestHordeIntro()
        {
            Dinosaur louis = new Dinosaur("Louis", 12, "Stegausaurus");
            Dinosaur nessie = new Dinosaur("Nessie", 11, "Diplodocus");
            Horde grp = new Horde();
            grp.add(louis);
            grp.add(nessie);

            Assert.Equal("Je suis Louis le Stegausaurus, j'ai 12 ans.\nJe suis Nessie le Diplodocus, j'ai 11 ans.\n", grp.intro());
        }*/
        [Fact]
        public void TestLabo(){
            Dinosaur louis=Laboratory.createDinosaur("Louis","Stego");
            Assert.Equal("Je suis Louis le Stego, j'ai 0 ans.",louis.sayHello());
            Dinosaur henry=Laboratory.createDinosaur("henry","diplo");
            Assert.Equal("Je suis Louis le diplo, j'ai 0 ans.",louis.sayHello());
            Assert.Equal("Je suis henry le diplo, j'ai 0 ans.",henry.sayHello());
        }
    }
}
